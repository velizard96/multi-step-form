let stepForm = new Vue({
  el: '#formSteps',
  data: {

    step: 1,
    totalSteps: 3,
    selectOption: 0,
    cardPrice: '10',
    sofanAdvertising: 'fr',
    phoneNumber: '',
    address: '',
    errors: false,
    errorMessage: '',
    
    form: {
      name: '',
      email: '',
      reg: '',
      quantity: '1',
      deliveryMethod: 'Магазин',
      hasErrors: false,
      deliveryToAddressPrice: 6,
      deliveryToAddress: 'Доставка до адрес',
      ekontPrice: 4.5,
      ekont: 'Еконт'
    }
  },
  methods: {
    nextStep: function () {
      if (  !this.form.name || !this.form.email) {
        this.form.hasErrors = true;
        this.errorMessage = "* Please fill out all inputs"
        return
      }
      this.errorMessage = "";
      this.hasErrors = false;
      this.step++

    },
    validateNumber: function() {
      if(!this.phoneNumber){
        this.errors = true;
        this.errorMessage = "* Please fill out all inputs"
        return
      }
      this.errorMessage = "";
      this.errors = false;
      this.step++
    },
    validateAddress: function() {
      if(!this.phoneNumber || !this.address){
        this.errors = true;
        this.errorMessage = "* Please fill out all inputs"
        return
      }
      this.errorMessage = "";
      this.errors = false;
      this.step++
    },
    prevStep: function () {
      this.step--
    },
    submitIt: function () {
      alert('Потвърдено')
    },
    onChange: function () {
      if (this.form.deliveryMethod == "Магазин") {
        return this.selectOption = 0
      }
      if (this.form.deliveryMethod == this.form.deliveryToAddress) {
        return this.selectOption = 6
      }
      if (this.form.deliveryMethod == this.form.ekont) {
        return this.selectOption = 4.5
      }
    },
    changeOption: function () {
      if (this.sofanAdvertising == "other") {
        return this.sofanAdvertising = 'other'
      }
    },
  },
  computed: {
    totalPrice: function () {
      return (parseFloat(this.cardPrice) * parseFloat(this.form.quantity)) + parseFloat(this.selectOption)
    },
    priceEuro: function () {
      return (parseFloat(this.totalPrice) / 1.9).toFixed(2)
    },
    deliveryToAddress: function () {
      return (parseFloat(this.cardPrice) * parseFloat(this.form.quantity)) + parseFloat(this.form.deliveryToAddress)
    }
  },
})
